package Util;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class ConvertJsonToObjectAbstract<T extends Object> {
	
	Class<T> enty;
	
	ConvertJsonToObjectAbstract(Class<T> enty){
		this.enty=enty;
	}
	
	public List<T> getArray(String json, String array) throws JSONException{
		JSONObject json_data = new JSONObject(json);
		JSONArray art = json_data.getJSONArray(array);
		
		Gson gson = new GsonBuilder().create();
		List<T> list = new ArrayList<T>();
		
		for (int i = 0; i < art.length(); i++) {
			JSONObject explrObject = art.getJSONObject(i);
			T ob = gson.fromJson(explrObject.toString(), enty);
			list.add(ob);
		}
		
		return list;
	}
}
