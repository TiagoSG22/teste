package WS;

import java.util.Set;
import javax.ws.rs.core.Application;


@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {
	
	@Override
	public Set<Class<?>> getClasses(){
		Set<Class<?>> res = new java.util.HashSet<>();
		addRestResourceClasses(res);
		return res;
	}

	private void addRestResourceClasses(Set<Class<?>> res) {
		res.add(CartsRestful.class);
	}

}
