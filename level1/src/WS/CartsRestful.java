package WS;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import org.codehaus.jettison.json.JSONException;

import Util.ConvertJsonToObjectArticles;
import Util.ConvertJsonToObjectCarts;
import model.Articles;
import model.Carts;
import model.Items;

@Path("/level1")
public class CartsRestful {
	
	private static final String ARTICLES="articles";
	private static final String CARTS="carts";
	
	ConvertJsonToObjectArticles art = new ConvertJsonToObjectArticles(Articles.class);
	ConvertJsonToObjectCarts car = new ConvertJsonToObjectCarts(Carts.class);
	
	/**
	 * Level 1
	 **/
	@POST
	@Path("/carts")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public List<modelR.Carts> getValorCarts(String json) throws JSONException{
		return calcularTotalCarts(json);
	}
	
	/**
	 * Calcula o valor total de cada carts
	 **/
	private List<modelR.Carts> calcularTotalCarts(String json) throws JSONException{
		
		List<Articles> articles = art.getArray(json, ARTICLES);
		List<Carts> cart = car.getArray(json, CARTS);
		
		List<modelR.Carts> carts = new ArrayList<modelR.Carts>();
		
		for(Carts c: cart){
			double total=0;
			
			for(Items i: c.getItems()){
				for(Articles a:articles){
					if(i.getArticle_id()==a.getId()){ 
						total += i.getQuantity()*a.getPrice();
					}
				}
			}
			carts.add(new modelR.Carts(c.getId(),total));
		}
		return carts;
	}
}
