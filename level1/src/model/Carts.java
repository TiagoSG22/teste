package model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;


@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Carts {
	
	@JsonProperty("id")
	private int id;
	
	@JsonProperty("items")
	private List<Items> items = new ArrayList<Items>();
	
	public Carts() {}

	public Carts(int id, List<Items> items) {
		this.id = id;
		this.items = items;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}	
}
