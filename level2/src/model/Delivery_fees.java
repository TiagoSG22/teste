package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Delivery_fees {
	
	@JsonProperty("eligible_transaction_volume")
	Eligible_transaction_volume eligible_transaction_volume;
	
	@JsonProperty("price")
	private double price;
	
	public Delivery_fees() {}

	public Delivery_fees(Eligible_transaction_volume eligible_transaction_volume, double price) {
		this.eligible_transaction_volume = eligible_transaction_volume;
		this.price = price;
	}

	public Eligible_transaction_volume getEligible_transaction_volume() {
		return eligible_transaction_volume;
	}
	public void setEligible_transaction_volume(Eligible_transaction_volume eligible_transaction_volume) {
		this.eligible_transaction_volume = eligible_transaction_volume;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
}
