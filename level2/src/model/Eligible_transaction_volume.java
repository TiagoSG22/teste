package model;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Eligible_transaction_volume {
	
	@JsonProperty("min_price")
	private Double min_price;
	@JsonProperty("max_price")
	private Double max_price;
	
	public Eligible_transaction_volume() {}
	
	public Eligible_transaction_volume(double min_price, double max_price) {
		this.min_price = min_price; //1000
		this.max_price = max_price; //2000
	}
	
	public double getMin_price() {
		return min_price;
	}
	public void setMin_price(double min_price) {
		this.min_price = min_price;
	}
	public double getMax_price() {
		return max_price;
	}
	public void setMax_price(double max_price) {
		this.max_price = max_price;
	}
	
	
	/**
	 * true: se tiver entre os valores min e max.
	 * false: se n�o tiver
	 **/
	@JsonIgnore
	public boolean Testar(double total){
		
		if(max_price == null && total >= min_price){
			return true;
		}
		
		if(total >= min_price && total < max_price){
			return true;
		}
		
			return false;
	}

}
