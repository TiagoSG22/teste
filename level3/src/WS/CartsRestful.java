package WS;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONException;

import Util.ConvertJsonToObjectArticles;
import Util.ConvertJsonToObjectCarts;
import Util.ConvertJsonToObjectDelivery_fees;
import Util.ConvertJsonToObjectDiscounts;
import model.Articles;
import model.Carts;
import model.Delivery_fees;
import model.Discounts;
import model.Items;

@Path("/level3")
public class CartsRestful {
	
	private static final String ARTICLES="articles";
	private static final String CARTS="carts";
	private static final String DELIVERY_FEES="delivery_fees";
	private static final String DISCOUNTS="discounts";
	
	ConvertJsonToObjectArticles art = new ConvertJsonToObjectArticles(Articles.class);
	ConvertJsonToObjectCarts car = new ConvertJsonToObjectCarts(Carts.class);
	ConvertJsonToObjectDelivery_fees del = new ConvertJsonToObjectDelivery_fees(Delivery_fees.class);
	ConvertJsonToObjectDiscounts dis = new ConvertJsonToObjectDiscounts(Discounts.class);
	
	/**
	 * Level 3
	 **/
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/carts")
	public List<modelR.Carts> getValorCarts(String json) throws JSONException{
		return calcularTotalCarts(json);
	}
	
	/**
	 * Calcula o valor total de cada carts e aplica o desconto caso tenha e o valor frete
	 * de acordo com as regras Delivery_fees recebido por json.
	 **/
	private List<modelR.Carts> calcularTotalCarts(String json) throws JSONException{
				
		List<Articles> articles = art.getArray(json, ARTICLES);
		List<Carts> cart = car.getArray(json, CARTS);
		List<Delivery_fees> delivery_fees = del.getArray(json, DELIVERY_FEES);
		List<Discounts> Discounts = dis.getArray(json, DISCOUNTS);
				
		List<modelR.Carts> carts = new ArrayList<modelR.Carts>();
		
		for(Carts c: cart){
			double total=0;
			
			for(Items i: c.getItems()){
				for(Articles a:articles){
					if(i.getArticle_id()==a.getId()){ 
						total += i.getQuantity()*(getPrice(a,Discounts));
					}
				}
			}
			total+=CalcularFrete(delivery_fees, total);
			carts.add(new modelR.Carts(c.getId(),total));
		}
		return carts;
	}
	
	
	/**
	 * Calcula o Frete de Acordo com Delivery_fees
	 **/
	private double CalcularFrete(List<Delivery_fees> delivery_fees, double total){
		
		double valor=0;
		
		for(Delivery_fees df: delivery_fees){
				if(df.getEligible_transaction_volume().Testar(total)){
					return (valor+=df.getPrice());
				}
		}
		
		return valor;
	}
	
	/**
	 * Calcula o Desconto de Acordo com type de Discounts
	 * retorna o novo valor do Articles
	 **/
	private double getPrice(Articles a, List<Discounts> Discounts){
		
		for(Discounts di: Discounts){
			if(a.getId()==di.getArticle_id())
				return di.CalcularDesconto(a);
		}
		return a.getPrice();
	}

}
