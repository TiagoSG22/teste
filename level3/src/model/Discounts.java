package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Discounts {
	
	@JsonProperty("article_id")
	private int article_id;
	@JsonProperty("type")
	private String type;
	@JsonProperty("value")
	private int value;
	
	public Discounts() {
	}
	
	public Discounts(int article_id, String type, int value) {
		this.article_id = article_id;
		this.type = type;
		this.value = value;
	}
	
	public int getArticle_id() {
		return article_id;
	}
	public void setArticle_id(int article_id) {
		this.article_id = article_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	/**
	 * Calcula o Desconto
	 * Retorna o novo valor do Articles j� com desconto aplicado
	 * Se n�o tiver retorna o valor original
	 **/
	public double CalcularDesconto(Articles a){
		
		if(this.type.equals("amount")){
			double aux = a.getPrice()-this.value;
			return aux;
		}
		
		if(this.type.equals("percentage")){
			String d = "0."+this.value;
			double pro = Double.parseDouble(d);
			double des = (a.getPrice()*pro);
			
			return (a.getPrice()-des);
		}
		
		return a.getPrice();
	}
}
